﻿using BSF.BaseService.NScript.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSF.BaseService.NScript
{
    public partial class FrTest : Form
    {
        public FrTest()
        {
            InitializeComponent();

            this.richTextBox1.Text = @" public class B
        {
            //static void Main(string[] args)
            //{
            //    System.Console.WriteLine(""hello"");
            //    System.Console.ReadLine();
            //    System.Console.ReadKey();
            //}
            public string test(string a)
            {
                return a;
            }
        }";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string msg = @"";
            try
            {
                string code = this.richTextBox1.Text;
                CompilerResult result = null;
                var r = NScriptHelper.Run<string>(new CompilerParams()
                {
                    EnumSourceType = Core.EnumSourceType.Code,
                    EnumCompilerMode = Core.EnumCompilerMode.Assembly,
                    CodeOrFileName = code
                },
                     "B", "test", new object[] { "a" }, out result);
                MessageBox.Show(r);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }



        public class B
        {
            //static void Main(string[] args)
            //{
            //    System.Console.WriteLine("hello");
            //    System.Console.ReadLine();
            //    System.Console.ReadKey();
            //}
            public string test(string a)
            {
                return a;
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {
            string msg = @"";
            try
            {
                string code = this.richTextBox1.Text;
                CompilerResult result = null;
                var r = NScriptHelper.Run<string>(new CompilerParams()
                {
                    EnumSourceType = Core.EnumSourceType.Code,
                    EnumCompilerMode = Core.EnumCompilerMode.AppDomian,
                    CodeOrFileName = code
                },
                     "B", "test", new object[] { "a" }, out result);
               
                MessageBox.Show(r);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
