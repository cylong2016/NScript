﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    public class Config
    {
        public static List<string> SystemReferencedAssembliesDlls = new List<string>(new string[]{
            "System.dll",
            "System.Core.dll",
            "System.Configuration.dll",
            "System.Data.dll",
            "System.Data.DataSetExtensions.dll",
            "System.Deployment.dll",
            "System.Drawing.dll",
            "System.Messaging.dll",
            "System.Net.Http.dll",
            "System.Web.dll",
            "System.Web.Extensions.dll",
            "System.Windows.Forms.dll",
            "System.Xml.dll",
            "System.Xml.Linq.dll",
            "Microsoft.CSharp.dll",
            "mscorlib.dll",
            "System.Runtime.Serialization.dll",
            "System.ServiceModel.dll",
        });
    }
}
